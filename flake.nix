{
  description = "Minimal GOG Galaxy compatible daemon to allow users to authorize with games that use the Galaxy service.";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-20.09";

  outputs = { self, nixpkgs }: {

    packages.x86_64-linux.comet = let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in pkgs.stdenv.mkDerivation {
      name = "comet";
      src = self;
      buildInputs = with pkgs; [ cmake curl protobuf ];
    };

    defaultPackage.x86_64-linux = self.packages.x86_64-linux.comet;

  };
}
